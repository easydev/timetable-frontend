class HomeCtrl {
    constructor(Stops, AppConstants, $scope) {
        'ngInject';

        this.appName = AppConstants.appName;

        Stops
            .getAll()
            .then(
                (stops) => {
                    this.stopsLoaded = true;
                    this.stops = stops;
                }
            );
    }
}

export default HomeCtrl;
