import angular from 'angular';

import constants  from './config/app.constants';
import appConfig  from './config/app.config';
import appRun     from './config/app.run';
import '@uirouter/angularjs';
import './config/app.templates';
import 'angular-ui-bootstrap';


import './home';
import './timetable';
import './services';

// Create and bootstrap application
const requires = [
    'ui.bootstrap.timepicker',
    'ui.bootstrap.tpls',
    'ui.router',
    'templates',
    'app.timetable',
    'app.home',
    'app.services'
];

window.app = angular.module('app', requires);
angular.module('app').constant('AppConstants', constants);
angular.module('app').config(appConfig);
angular.module('app').run(appRun);

angular.bootstrap(document, ['app'], {
    strictDi: true
});