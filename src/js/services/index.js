import angular from 'angular';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);


import StopsService from './stops.service';
servicesModule.service('Stops', StopsService);

import TimetableService from './timetable.service';
servicesModule.service('Timetable', TimetableService);

export default servicesModule;
