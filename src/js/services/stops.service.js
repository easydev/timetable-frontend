export default class Stops {
    constructor(AppConstants, $http, $q) {
        'ngInject';

        this._AppConstants = AppConstants;
        this._$http = $http;
        this.stops = [];
    }

    getAll() {
        return this._$http({
            url: this._AppConstants.api,
            method: 'GET',
        }).then((res) => this.stops = res.data);
    }

    getStopById(stopId) {
        var item = this.stops.find(item => item.id === stopId);
        return item.stop_name;
    }
}