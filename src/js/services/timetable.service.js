export default class Timetable {
    constructor(AppConstants, $http, $q) {
        'ngInject';

        this._AppConstants = AppConstants;
        this._$http = $http;
    }

    getTimetableByStopId(stopId) {
        return this._$http({
            url: this._AppConstants.api + "/stop/" + stopId,
            method: 'GET',
        }).then((res) => res.data);
    }

    getDeparturesForTime(stopId, timeHours, timeMinutes) {
        return this._$http({
            url: this._AppConstants.api + "/departures/stop/" + stopId + '/' + timeHours + '/' + timeMinutes,
            method: 'GET',
        }).then((res) => res.data);
    }
}