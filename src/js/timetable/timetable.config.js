import TimetableCtrl from "./timetable.controller";

function TimetableConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('app.timetable', {
            url: '/timetable/{stopId}',
            controller: 'TimetableCtrl',
            controllerAs: '$ctrl',
            templateUrl: 'timetable/timetable.html',
            title: 'Stop timetable'
        });
};

export default TimetableConfig;
