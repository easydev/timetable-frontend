class TimetableCtrl {
    constructor(Stops, Timetable, AppConstants, $scope, $stateParams) {
        'ngInject';

        this.appName = AppConstants.appName;
        this.stopName = Stops.getStopById(parseInt($stateParams.stopId));

        $scope.changed = function () {
            Timetable
                .getDeparturesForTime($stateParams.stopId, $scope.departureTime.getHours(), $scope.departureTime.getMinutes())
                .then(
                    (departures) => {
                        $scope.departuresLoaded = true;
                        $scope.departures = departures;
                    }
                )
        };

        Timetable
            .getTimetableByStopId($stateParams.stopId)
            .then(
                (timetable) => {
                    $scope.timetableLoaded = true;
                    $scope.timetable = timetable;
                }
            );

        $scope.departureTime = new Date();
        $scope.changed();
        $scope.hstep = 1;
        $scope.mstep = 1;
    }
}

export default TimetableCtrl;
