import angular from 'angular';

let timetableModule = angular.module('app.timetable', []);

import TimetableConfig from './timetable.config';
timetableModule.config(TimetableConfig);

import TimetableCtrl from './timetable.controller';
timetableModule.controller('TimetableCtrl', TimetableCtrl);


export default timetableModule;
